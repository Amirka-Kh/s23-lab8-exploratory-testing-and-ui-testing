from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

driver = webdriver.Firefox()
driver.implicitly_wait(10) # seconds

start = time.time()

driver.get("http://youtube.com")
print("Page title is: "+driver.title)
assert "YouTube" in driver.title

elem = driver.find_element(By.NAME, "search_query")
elem.clear()
elem.send_keys("SQR course is the best")
elem.send_keys(Keys.RETURN)


elem = driver.find_element(By.ID, "contents")
assert elem.is_displayed()

end = time.time()

print(end - start)
driver.close()
