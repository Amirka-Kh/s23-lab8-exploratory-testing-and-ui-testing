import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


driver = webdriver.Firefox()
driver.implicitly_wait(10) # seconds


def stream(VideoName):
	driver.get(f"https://m.youtube.com/results?search_query={VideoName}")
	driver.implicitly_wait(3)
	video = driver.find_element(
		"xpath", "//*[@id='video-title'][1]")
	video.click()


def pauseAndPlay():
	driver.implicitly_wait(3)
	pauseVideo = driver.find_element("xpath", "/html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[1]/div[2]/div/div/ytd-player/div/div/div[1]/video")
	driver.implicitly_wait(1)
	pauseVideo.click()
	playVideo = driver.find_element(
	"xpath", "/html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[1]/div[2]/div/div/ytd-player/div/div/div[28]/div[2]/div[1]/button")
	assert playVideo.get_attribute("title") == 'Play (k)'


def video():
	video = driver.find_element(
		"xpath", "/html/body/ytd-app/div[1]/ytd-page-manager/\
		ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/\
		div/div[1]/h1/yt-formatted-string")
	
	author = driver.find_element(
		"xpath", "/html/body/ytd-app/div[1]/ytd-page-manager\
		/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata\
		/div/div[2]/div[1]/ytd-video-owner-renderer/div[1]/\
		ytd-channel-name/div/div/yt-formatted-string/a")
	assert video.text == "Ost Anne Happy - Michino Timothy Kimino Kimochi"


start = time.time()

stream('ti ti timo timo')
video()
pauseAndPlay()

end = time.time()
print(end - start)
driver.close()
